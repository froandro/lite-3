#! /usr/bin/env bash

# Install PHP
echo "----- Install PHP ------"
sudo apt-get update
sudo apt-get install php7.4-fpm php-mysqlnd php-pgsql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y
sudo systemctl restart php7.4-fpm

# Install Wordpress
echo "----- Install Wordpress ------"
cd /tmp
curl -LO https://wordpress.org/wordpress-5.0.tar.gz
tar xzvf wordpress-5.0.tar.gz
#curl -LO https://wordpress.org/latest.tar.gz
#tar xzvf latest.tar.gz
cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php
sudo cp -a /tmp/wordpress/. /var/www/wordpress
sudo chown -R www-data:www-data /var/www/wordpress


# Config connect Wordpress Database
echo "----- Config connect Wordpress Database------"
sed -i "s#database_name_here#$wp#" /var/www/wordpress/wp-config.php
sed -i "s#username_here#$wp_admin#" /var/www/wordpress/wp-config.php
sed -i "s#password_here#$wp_pass#" /var/www/wordpress/wp-config.php
sed -i "s#localhost#$ip_db1#" /var/www/wordpress/wp-config.php

# Change unique phrases
echo "----- Change unique phrases ------"
curl -s https://api.wordpress.org/secret-key/1.1/salt/ -o /var/www/wordpress/.secrets
cd /var/www/wordpress
sed -i -e "/NONCE_SALT/r .secrets" wp-config.php
sed -i "/put your unique phrase here/d" wp-config.php

# Install PG4WP
echo "----- Install PG4WP ------"
cd wp-content
git clone https://github.com/kevinoid/postgresql-for-wordpress.git
mv postgresql-for-wordpress/pg4wp Pg4wp
cp Pg4wp/db.php db.php
#rm -rf postgresql-for-wordpress
#rm ../../latest.tar.gz

# Install Nginx
echo "----- Install Nginx ------"
sudo apt-get install nginx -y

cat > /etc/nginx/sites-available/wordpress <<EOF
server {
        listen 80;
        listen [::]:80;
        root /var/www/wordpress;
        index  index.php index.html index.htm;
        server_name mysite.com www.mysite.com;

        error_log /var/log/nginx/mysite.com_error.log;
        access_log /var/log/nginx/mysite.com_access.log;

        client_max_body_size 100M;
        location / {
                try_files \$uri \$uri/ /index.php?\$args;
        }
        location ~ \.php\$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.4-fpm.sock;
                fastcgi_param   SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        }
}
EOF

sudo ln -s /etc/nginx/sites-available/wordpress /etc/nginx/sites-enabled/
sudo unlink /etc/nginx/sites-enabled/default
sudo nginx -t
sudo systemctl restart nginx
echo "----- SUCCESSFUL -----"

