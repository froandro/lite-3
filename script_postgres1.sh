#! /usr/bin/env bash

# Install Postgres_Server
echo "----- Install Postgres_Server ------"
sudo apt update
sudo apt install postgresql postgresql-contrib -y

# Config Master_Server
echo "----- Config Master_Server ---------"
sudo -u postgres psql -c "ALTER ROLE postgres PASSWORD '$pass'"
sed -i "$ a\host replication postgres $ip_db2/32 md5" /etc/postgresql/12/main/pg_hba.conf
sed -i "/IPv4 local connections:/a host all all $ip_web/32 md5" /etc/postgresql/12/main/pg_hba.conf

sed -i "/listen/c listen_addresses = 'localhost, $ip_db1'" /etc/postgresql/12/main/postgresql.conf
sed -i "/wal_level/c wal_level = hot_standby" /etc/postgresql/12/main/postgresql.conf
sed -i "/archive_mode/c archive_mode = on" /etc/postgresql/12/main/postgresql.conf
sed -i "/archive_command/c archive_command = 'cd .'" /etc/postgresql/12/main/postgresql.conf
sed -i "/max_wal_senders/c max_wal_senders = 8" /etc/postgresql/12/main/postgresql.conf
sed -i "/hot_standby/c hot_standby = on" /etc/postgresql/12/main/postgresql.conf

# Create DB for Wordpress
echo "-------- Create DB for Wordpress -------"
sudo -u postgres psql -c "create database $wp;"
sudo -u postgres psql -c "create user $wp_admin with password '$wp_pass';"
sudo -u postgres psql -c "grant all privileges on database $wp to $wp_admin;"

sudo service postgresql restart
echo "----- SUCCESSFUL -----"

