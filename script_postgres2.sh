#! /usr/bin/env bash

# Install Postgres_Server
echo "----- Install Postgres_Server ------"
sudo apt update
sudo apt install postgresql postgresql-contrib -y

# Config Replica_Server
echo "----- Config Replica_Server ---------"
sudo service postgresql stop
sed -i "$ a\host replication postgres $ip_db1/32 md5" /etc/postgresql/12/main/pg_hba.conf
sed -i "/IPv4 local connections:/a host all all $ip_web/32 md5" /etc/postgresql/12/main/pg_hba.conf

sed -i "/listen/c listen_addresses = 'localhost, $ip_db2'" /etc/postgresql/12/main/postgresql.conf
sed -i "/wal_level/c wal_level = hot_standby" /etc/postgresql/12/main/postgresql.conf
sed -i "/archive_mode/c archive_mode = on" /etc/postgresql/12/main/postgresql.conf
sed -i "/archive_command/c archive_command = 'cd .'" /etc/postgresql/12/main/postgresql.conf
sed -i "/max_wal_senders/c max_wal_senders = 8" /etc/postgresql/12/main/postgresql.conf
sed -i "/hot_standby/c hot_standby = on" /etc/postgresql/12/main/postgresql.conf

# Create/copy databases from master
echo "------ Create/copy new database from master --------"
sudo su postgres -c "echo $ip_db1:*:*:postgres:$pass > ~/.pgpass"
sudo su postgres -c "chmod 0600 ~/.pgpass"
sudo su postgres -c "rm -rf /var/lib/postgresql/12/main"
sudo su postgres -c "mkdir /var/lib/postgresql/12/main"
sudo su postgres -c "chmod go-rwx /var/lib/postgresql/12/main"
sudo su postgres -c "pg_basebackup -P -R -X stream -c fast -h $ip_db1 -U postgres -w -D /var/lib/postgresql/12/main"

sudo service postgresql start
echo "----- SUCCESSFUL -----"

